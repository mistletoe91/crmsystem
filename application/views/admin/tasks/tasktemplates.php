<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">

      <div class="panel_s">
          <div class="panel-body _buttons">
                            <h2>Task Templates</h2>
                            <div class="alert alert-info" role="alert">Use following table to add/view/insert/delete task templates</div>
              </div>
       </div>

        <div class="panel_s">
          <div class="panel-body">

             <div class="row mbot15">
                <div class="col-md-12">
                  <h3 class="text-success no-margin">Task Templates</h3>
                </div>
             </div>
             <div class="clearfix"></div>

          <div id="TemplateTasks" style="width:  ;"></div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<script>
		$(document).ready(function () {

		    //Prepare jTable
			$('#TemplateTasks').jtable({
				title: 'Tasks',
				paging: true,
				pageSize: 20,
				sorting: true,
				defaultSorting: 'name ASC',
				actions: {
					listAction: '/template_api.php?do=task&action=list',
					createAction: '/template_api.php?do=task&action=create',
					updateAction: '/template_api.php?do=task&action=update',
					deleteAction: '/template_api.php?do=task&action=delete'
				},
				fields: {
					id: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					name: {
						title: 'Task Template Name',
						width: '20%'
					},
					description: {
						title: 'description',
					},
					billable: {
						title: 'Billable?',
						options: { '0': 'Non Billable', '1': 'Billable' }
					},
					is_public: {
						title: 'Public?',
						options: { '0': 'Private', '1': 'Public' }
					},
					status: {
						title: 'Default Status',
						options: { '1': 'Not Started', '2': 'Awaiting Feedback' , '3': 'Testing', '4': 'In Progress', '5': 'Complete'}
					},
					priority: {
						title: 'Default Priority',
						options : '/template_api.php?do=generic&action=getpriority'
					},
          period_end_mm: {
						title: "Period End Month",
				                 options : "/template_api.php?do=generic&action=getmonths"
					},
					period_end_dd: {
						title: "Period End Day",
						dependsOn: "period_end_mm",
                                                options: function (data) {
                                                    if (data.source == "list") {
                                                        //Return url of all countries for optimization.
                                                        //This method is called for each row on the table and jTable caches options based on this url.
                                                        return "/template_api.php?do=generic&action=getdayofmonthbyrecid&recid="+data.record.id;
                                                    }

                                                    //This code runs when user opens edit/create form or changes continental combobox on an edit/create form.
                                                    //data.source == "edit" || data.source == "create"
                                                    return "/template_api.php?do=generic&action=getdayofmonthbymonthid&period_end_mm=" + data.dependedValues.period_end_mm;
                                                }
					} ,
					template_duration: {
						title: 'Duration (Relative To Period End)',
            options: {
              '900': '1st Calendar Day of Next Month (Max 1 Day)',
              '901': '15th Calendar Day of Next Month (Max 15 Days)',
              '99': 'Last Calendar Day of Next Month (Max 31 Days)',
            }
					},
					hourly_rate: {
						title: 'Hourly rate',
					},
					assigned_to: {
						title: "Default Assignee",
                                                options: function (data) {
                                                    if (data.source == "list") {
                                                        //Return url of all countries for optimization.
                                                        //This method is called for each row on the table and jTable caches options based on this url.
                                                        return "/template_api.php?do=generic&action=getallstaff";
                                                    }

                                                    //This code runs when user opens edit/create form or changes continental combobox on an edit/create form.
                                                    //data.source == "edit" || data.source == "create"
                                                    return "/template_api.php?do=generic&action=getallstaff";
                                                }
					},
					checklistitem: {
						title: 'Checklist Items <small>One item per line</small>',
						type : 'textarea',
						list: false
					}
				}
			});

			//Load person list from server
			$('#TemplateTasks').jtable('load');

		});
</script>
<link href="/assets/plugins/jtable/themes/lightcolor/blue/accerp.css" rel="stylesheet" type="text/css" />
<script src="/assets/plugins/jtable/jquery.jtable.2.4.0.js" type="text/javascript"></script>
</body>
</html>
